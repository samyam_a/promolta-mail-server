var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var app = express();


app.use(logger('dev'));
// app.use(express.json());
// app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

var expressPath = require('express-path');
var routes = require('./routes');
expressPath(app, routes);

app.use(function (err, req, res, next) {
  if (!err) {
    err = {};
  }
  console.log(error);
  res.status(err.status || 500);
  res.json(response_error);
})



module.exports = app;
