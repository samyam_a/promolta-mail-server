var gulp = require('gulp');
var gutil = require('gulp-util');
var nodemon = require('gulp-nodemon');

var runTimestamp = Math.round(Date.now() / 1000);

gulp.task('default', ['nodemon'],function (done) {
    // runSequence(function () {
    //     done();
    // });
    done();
});

gulp.task('nodemon', function (cb) {
    
    var started = false;
    
    return nodemon({
        script: './bin/www',
        exec: 'node --inspect --debug=5736'
    }).on('start', function () {
        // to avoid nodemon being started multiple times
        // thanks @matthisk
        if (!started) {
            cb();
            started = true; 
        } 
    });
});

function onError(err) {
    gutil.log(err);
    this.emit('end');
}