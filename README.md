# Installation Instructions 

Please run on nodeV8 LTS (8.9.x)

Ensure npm is atleast v6.0.x (`npx` command is available in this version)

`npm install`

`npx gulp`
