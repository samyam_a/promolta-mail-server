module.exports = [
    ['/', 'index#index'],
    ['/users', 'users#getAll', 'get'],
    ['/users/validate','users#validateUser', 'utils#jsonParser', 'post'],
    ['/messages','messages#getMessages', 'utils#jsonParser', 'post'],
    ['/messages/:msg_id/send','messages#sendMessage', 'utils#jsonParser', 'post'],
    ['/messages/:msg_id/read','messages#readMessage', 'utils#jsonParser', 'post'],
    ['/messages/:msg_id','messages#getMessage', 'utils#jsonParser', 'get'],
    ['/messages/:msg_id/view','messages#viewMessage', 'utils#jsonParser', 'get'],
    ['/messages/update','messages#updateMessage', 'utils#jsonParser', 'post'],
    ['/messages/create','messages#newMessage', 'utils#jsonParser', 'post'],
];