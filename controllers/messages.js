var _ = require('lodash');
var q = require('q');
var MessageService = require('../services/MessageService');

var req_defaults = {
    page: 1,
    count: 20,
    sort_list:[],
    filter_list:[]
}



function _getMessages(req, res, next){
    var user_id = _.get(req.body, "user_id", null);
    var box_id = _.get(req.body,"box_id", 1); // default to inbox
    var page = _.get(req.query,"page", req_defaults.page);
    var count = _.get(req.query,"count", req_defaults.count);

    if(user_id)
        return MessageService.getMessagesForUser(user_id, box_id, page, count)
        .then(function(user_messages){
            res.json(user_messages);
        });
    else
        res.sendStatus(400, "Invalid Request. No User ID");
}

function _getMessage(req, res, next){
    var msg_id = _.get(req.params, "msg_id");

    return MessageService.getMessageById(msg_id)
    .then(function(message){
        res.json(message);
    });
}

function _viewMessage(req, res, next){
    var msg_id = _.get(req.params, "msg_id");

    return MessageService.getMessageThread(msg_id)
    .then(function(message){
        res.json(message);
    });
}



function _newMessage(req, res){
    var user_id = _.get(req.body, "user_id");
    var parent_id = _.get(req.body,"parent_id", null);
    var is_forward = _.get(req.body,"is_forward", null);

    if(!user_id){
        res.sendStatus(400);
        return;
    }

    MessageService.createNewMessage(user_id, parent_id, is_forward)
    .then(function(new_msg){
        res.json(new_msg);
    })
}

function _updateMessage(req, res){
    var msg = req.body;

    MessageService.updateMessage(msg)
    .then(function(new_msg){
        res.json(new_msg);
    })
}

function _sendMessage(req, res){
    var msg_id = _.get(req.params, 'msg_id');

    MessageService.sendMessage(msg_id)
    .then(function(new_msg){
        res.sendStatus(200);
    }).catch(function(error){
        res.sendStatus(500);
    })
}

function _readMessage(req, res){
    var msg_id = _.get(req.params, 'msg_id');
    var user_id = _.get(req.body, "user_id");

    MessageService.readMessage(msg_id, user_id)
    .then(function(new_msg){
        res.sendStatus(200);
    }).catch(function(error){
        res.sendStatus(500);
    })
}

module.exports = {
    getMessages: _getMessages,
    getMessage: _getMessage,
    newMessage: _newMessage,
    updateMessage: _updateMessage,
    sendMessage: _sendMessage,
    viewMessage: _viewMessage,
    readMessage: _readMessage,
}