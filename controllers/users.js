var _ = require('lodash');
var Users = require('../db/models').Users;
var bcrypt = require('bcrypt');


function _getAll(req, res, next){
    var project_id = req.params.project_id;

    Users.findAll()
    .then(function(users){
        res.json(users.toJSON());
    }).catch(function(error){
        next(error);
    })
}

function _validateUser(req, res, next){
    var email = req.body.email;
    var password = req.body.password;

    Users.forge({'email':email})
    .fetch()
    .then(function(user){
        var hash = user.get('pwd_hash');
        bcrypt.compare(password, hash, function(err, result) {
            if(result  == true){
                res.json(user)
            }else{
                res.sendStatus(403);
            }
        });
    }).catch(function(error){
        next(error);
    })
}

module.exports = {
    getAll:_getAll,
    validateUser: _validateUser
};
