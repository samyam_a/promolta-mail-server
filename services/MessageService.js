var _ = require('lodash');
var q = require('q');
var Users = require('../db/models').Users;
var Message = require('../db/models').Message;
var MessageParents = require('../db/models').MessageParents;
var UserMessages = require('../db/models').UserMessages;

function __getMessagesForUser(user_id, box_id, page, count){
    return UserMessages.query(function(qb){
        qb.where({user_id: user_id, message_box_id: box_id})
    })
    .orderBy('-updated_at')
    .fetchPage({page: page, pageSize: count, withRelated: ['Message', 'Message.sender']});
}

function __getMessageById(message_id){
    return Message.findById(message_id,{withRelated:['sender']})
}

function __getMessageThread(message_id){
    var usr_msg = null;
    return __getMessageById(message_id)
    .then(function(msg){
        usr_msg = msg;
        return MessageParents.query(function(qb){
            qb.where({message_id: message_id})
        })
        .orderBy('id')
        .fetchAll()
    })
    .then(function(msg_parents){
        var id_array = msg_parents.map(function(mp){return mp.get('parent_message_id')});
        var promises = [];
        var msg_parents = {};
        _.each(id_array,function(msg_id){
            promises.push( __getMessageById(msg_id).then( function(m){_.set(msg_parents,msg_id,m.serialize())} ) );
        })
        return q.all(promises)
        .then(function(){
            var ordered_msg_parents = [];
            _.each(id_array, function(id){ordered_msg_parents.push( _.get(msg_parents, id ))})
            return ordered_msg_parents
        });
    })
    .then(function(msg_parents){
        return msg_parents.reduceRight(function(flattened, msg_parent) {
          return _.set(msg_parent,'child_msg',flattened);
        }, null);
    }).then(function(thread){
        return _.set(usr_msg.serialize(), 'child_msg', _.get(thread, 'child_msg'))
    })
}

function __createNewMessage(user_id, parent_id, is_forward=false){
    var msg = {};
    var defer = q.defer();
    var call_promise = defer.promise;
    

    _.set(msg, 'author_id', user_id);
    if(parent_id){
        call_promise = call_promise.then(function(){
            return __getMessageById(parent_id);
        }).then(function(parent_message){
            _.set(msg, 'subject_line', parent_message.get('subject_line'));
            if(!is_forward){
                _.set(msg, 'mail_to', parent_message.related('sender').get('email'));
            }
        })
    }
    call_promise = call_promise.then(function(){
        return Message.forge(msg).save(null, {method: 'insert', required: true});
    })
    .then(function(new_msg){
        return UserMessages.forge({user_id: user_id, message_id: new_msg.get('id'), message_box_id: 3}).save()
        .then(function(user_msg){
            if(parent_id){
                return MessageParents.forge({message_id: new_msg.get('id'), parent_message_id: parent_id}).save()
                .then(function(msg_parent){
                    return MessageParents.query(function(qb){
                        qb.where({message_id: parent_id})
                    })
                    .orderBy('id')
                    .fetchAll()
                })
                .then(function(msg_parents){
                    var promises = [];
                    msg_parents.each(function(msg_parent){
                        var older_parent_id = msg_parent.get('parent_message_id');
                        promises.push(MessageParents.forge({message_id: new_msg.get('id'), parent_message_id: older_parent_id}).save());
                    })
                    return q.all(promises);
                })
            }
        })
        .then(function(){
            return new_msg.refresh();
        });
    }).catch(function(error){
        throw(error);
    });

    defer.resolve();
    return call_promise;
}

function __updateMessage(msg){
    var upd_msg = _.pick(msg, ['id','mail_to', 'subject_line', 'body']);
    if(_.get(upd_msg,'id', null)){
        return Message.forge({id: _.get(upd_msg,'id')}).save(upd_msg, {patch: true})
        .then(function(updated_msg){
            return updated_msg.refresh();
        });
    }else{
        return new Promise(function(resolve, reject){
            throw("Messge id not present")
        })
    }
}

function __readMessage(msg_id, user_id){
    return UserMessages.query(function(qb){
        qb.where({user_id: user_id, message_id: msg_id})
    })
    .fetch()
    .then(function(user_msg){
        user_msg.save({is_read:1},{patch:true})
    })
}

function __parseAddressesToUsers(mail_to){
    var addresses = _.split(mail_to,',');
    if(addresses.length <= 0) return null;
    addresses = _.map(addresses, _.trim);

    return Users.query(function(qb){
        qb.whereIn('email', addresses)
    }).fetchAll();
}

function __sendMessage(msg_id){
    var msg = null;
    var user_msg = null;
    return __getMessageById(msg_id)
    .then(function(msg_obj){
        msg = msg_obj;
        return UserMessages.query(function(qb){
            qb.where({user_id: msg.get('author_id'), message_id: msg_id})
        }).fetch();
    }).then(function(user_msg_mdl){
        if(user_msg_mdl) user_msg = user_msg_mdl;
        else throw("User Message Not Found")
    }).then(function(){
        if(user_msg.get('message_box_id') == 3){
            var mail_to = msg.get('mail_to');
            return __parseAddressesToUsers(mail_to);
        }else{
            throw('Message Cannot be sent as it is not a new message.')
        }
    }).then(function(users){
        var promises = [];
        users.each(function(user){
            var user_id = user.get('id');
            promises.push(UserMessages.forge({user_id: user_id, message_id: msg.get('id'), message_box_id: 1}).save(null,{method:'insert'}));
        })
        promises.push(user_msg.save({message_box_id: 2},{patch:true}));
        return q.all(promises);
    });
}

module.exports = {
    getMessagesForUser: __getMessagesForUser,
    getMessageById: __getMessageById,
    createNewMessage: __createNewMessage,
    updateMessage: __updateMessage,
    sendMessage: __sendMessage,
    getMessageThread: __getMessageThread,
    readMessage: __readMessage,
};