var bodyParser = require('body-parser');

exports.jsonParser = bodyParser.json({limit: '5mb'});
exports.urlEncodedParser = bodyParser.urlencoded({limit: '5mb', extended: false});