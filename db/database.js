var knex = require('knex')({
  client: 'mysql',
  connection: {
    host     : '127.0.0.1',
    user     : 'root',
    password : '',
    database : 'mailtest',
    charset  : 'utf8'
  },
  pool: {min:2, max:40},
  debug: true,
});
var bookshelf = require('bookshelf')(knex);

bookshelf.plugin(require('bookshelf-modelbase').pluggable);
bookshelf.plugin(require('bookshelf-paranoia'));
// bookshelf.plugin(require('bookshelf-json-columns'));

bookshelf.plugin('registry');
bookshelf.plugin('pagination');
bookshelf.plugin('virtuals');

module.exports = bookshelf;
