module.exports = {
    Users: require('./user'),
    Message: require('./message'),
    MessageParents: require('./message_parents'),
    UserMessages: require('./user_messages'),
}