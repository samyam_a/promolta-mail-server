var bookshelf = require('../database');
var moment = require('moment');

var MessageParents = bookshelf.Model.extend({
    tableName: 'message_parent_map',
    softDelete: true,
    parent_message: function(){
        this.belongsTo('Message','parent_message_id')
    },
    toJSON: function () {
        var attrs = bookshelf.Model.prototype.toJSON.apply(this, arguments);
    
        if( this.get('created_at'))
            attrs.created_at = moment(this.get('created_at')).format('YYYY-MM-DD HH:mm:ss');
        if( this.get('updated_at'))
            attrs.updated_at = moment(this.get('updated_at')).format('YYYY-MM-DD HH:mm:ss');
        if( this.get('deleted_at'))
            attrs.deleted_at = moment(this.get('deleted_at')).format('YYYY-MM-DD HH:mm:ss');

        return attrs;
    }
});

module.exports = bookshelf.model('MessageParents', MessageParents);