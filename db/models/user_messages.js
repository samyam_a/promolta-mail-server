var bookshelf = require('../database');
var moment = require('moment');

var UserMessages = bookshelf.Model.extend({
    tableName: 'user_message_map',
    softDelete: true,
    Message: function(){
        return this.belongsTo('Message', 'message_id')
    },

    toJSON: function () {
        var attrs = bookshelf.Model.prototype.toJSON.apply(this, arguments);
    
        if( this.get('created_at'))
            attrs.created_at = moment(this.get('created_at')).format('YYYY-MM-DD HH:mm:ss');
        if( this.get('updated_at'))
            attrs.updated_at = moment(this.get('updated_at')).format('YYYY-MM-DD HH:mm:ss');
        if( this.get('deleted_at'))
            attrs.deleted_at = moment(this.get('deleted_at')).format('YYYY-MM-DD HH:mm:ss');

        return attrs;
    }
});

module.exports = bookshelf.model('UserMessages', UserMessages);