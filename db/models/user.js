var bookshelf = require('../database');
var moment = require('moment');

var Users = bookshelf.Model.extend({
    tableName: 'users',
    softDelete: true,
    toJSON: function () {
        var attrs = bookshelf.Model.prototype.toJSON.apply(this, arguments);
    
        if( this.get('created_at'))
            attrs.created_at = moment(this.get('created_at')).format('YYYY-MM-DD HH:mm:ss');
        if( this.get('updated_at'))
            attrs.updated_at = moment(this.get('updated_at')).format('YYYY-MM-DD HH:mm:ss');
        if( this.get('deleted_at'))
            attrs.deleted_at = moment(this.get('deleted_at')).format('YYYY-MM-DD HH:mm:ss');

        return attrs;
    }
}, {
    fields: [
        'email',
        'first_name',
        'last_name'
    ]
});

module.exports = bookshelf.model('Users', Users);