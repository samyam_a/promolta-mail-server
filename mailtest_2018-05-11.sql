# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.7.22)
# Database: mailtest
# Generation Time: 2018-05-11 00:35:35 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table message
# ------------------------------------------------------------

DROP TABLE IF EXISTS `message`;

CREATE TABLE `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `mail_to` varchar(2048) DEFAULT NULL,
  `subject_line` varchar(1024) DEFAULT NULL,
  `body` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table message_box
# ------------------------------------------------------------

DROP TABLE IF EXISTS `message_box`;

CREATE TABLE `message_box` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `box_name` varchar(32) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `message_box` WRITE;
/*!40000 ALTER TABLE `message_box` DISABLE KEYS */;

INSERT INTO `message_box` (`id`, `box_name`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'INBOX','2018-05-09 21:30:57','2018-05-09 21:30:57',NULL),
	(2,'SENT','2018-05-09 21:30:57','2018-05-09 21:30:57',NULL),
	(3,'DRAFTS','2018-05-09 21:30:57','2018-05-09 21:30:57',NULL),
	(4,'TRASH','2018-05-09 21:30:57','2018-05-09 21:30:57',NULL);

/*!40000 ALTER TABLE `message_box` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table message_parent_map
# ------------------------------------------------------------

DROP TABLE IF EXISTS `message_parent_map`;

CREATE TABLE `message_parent_map` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `message_id` int(11) NOT NULL,
  `parent_message_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `message_id` (`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table user_message_map
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_message_map`;

CREATE TABLE `user_message_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `is_read` tinyint(4) DEFAULT '0',
  `message_box_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(64) NOT NULL,
  `pwd_hash` varchar(128) NOT NULL,
  `first_name` varchar(64) DEFAULT NULL,
  `last_name` varchar(64) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `email`, `pwd_hash`, `first_name`, `last_name`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'system@mailtest.com','$2b$10$uX8uN2BjOonn9Pv/4bj80emzEnTaCBiMcHWdSD1yW3NAY5d6cEYw.','System','Administrator','2018-05-09 22:14:22','2018-05-09 22:14:22',NULL),
	(2,'user1@mailtest.com','$2b$10$uX8uN2BjOonn9Pv/4bj80emzEnTaCBiMcHWdSD1yW3NAY5d6cEYw.','User1','Test','2018-05-09 22:14:22','2018-05-09 22:14:22',NULL),
	(3,'user2@mailtest.com','$2b$10$uX8uN2BjOonn9Pv/4bj80emzEnTaCBiMcHWdSD1yW3NAY5d6cEYw.','User2','Test','2018-05-09 22:14:22','2018-05-09 22:14:22',NULL),
	(4,'user3@mailtest.com','$2b$10$uX8uN2BjOonn9Pv/4bj80emzEnTaCBiMcHWdSD1yW3NAY5d6cEYw.','User3','Test','2018-05-09 22:14:22','2018-05-09 22:14:22',NULL),
	(5,'user4@mailtest.com','$2b$10$uX8uN2BjOonn9Pv/4bj80emzEnTaCBiMcHWdSD1yW3NAY5d6cEYw.','User4','Test','2018-05-09 22:14:22','2018-05-09 22:14:22',NULL),
	(6,'user5@mailtest.com','$2b$10$uX8uN2BjOonn9Pv/4bj80emzEnTaCBiMcHWdSD1yW3NAY5d6cEYw.','User5','Test','2018-05-09 22:14:22','2018-05-09 22:14:22',NULL),
	(7,'user6@mailtest.com','$2b$10$uX8uN2BjOonn9Pv/4bj80emzEnTaCBiMcHWdSD1yW3NAY5d6cEYw.','User6','Test','2018-05-09 22:14:22','2018-05-09 22:14:22',NULL),
	(8,'user7@mailtest.com','$2b$10$uX8uN2BjOonn9Pv/4bj80emzEnTaCBiMcHWdSD1yW3NAY5d6cEYw.','User7','Test','2018-05-09 22:14:22','2018-05-09 22:14:22',NULL),
	(9,'user8@mailtest.com','$2b$10$uX8uN2BjOonn9Pv/4bj80emzEnTaCBiMcHWdSD1yW3NAY5d6cEYw.','User8','Test','2018-05-09 22:14:22','2018-05-09 22:14:22',NULL),
	(10,'user9@mailtest.com','$2b$10$uX8uN2BjOonn9Pv/4bj80emzEnTaCBiMcHWdSD1yW3NAY5d6cEYw.','User9','Test','2018-05-09 22:14:22','2018-05-09 22:14:22',NULL),
	(11,'user10@mailtest.com','$2b$10$uX8uN2BjOonn9Pv/4bj80emzEnTaCBiMcHWdSD1yW3NAY5d6cEYw.','User10','Test','2018-05-09 22:14:22','2018-05-09 22:14:22',NULL);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
